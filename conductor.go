package conductor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/kpango/glg"
)

type LogResp struct {
	Log        string `json:"log"`
	CreateTime int64  `json:"createdTime"`
}

type ResponsePoll struct {
	WorkflowInstanceId string      `json:"workflowInstanceId"`
	TaskId             string      `json:"taskId"`
	InputData          interface{} `json:"inputData"`
}

type ResponseTask struct {
	WorkflowInstanceId    string      `json:"workflowInstanceId"`
	TaskId                string      `json:"taskId"`
	ReasonForIncompletion string      `json:"reasonForIncompletion"`
	CallbackAfterSeconds  int         `json:"callbackAfterSeconds"`
	WorkerId              string      `json:"workerId"`
	Status                string      `json:"status"`
	OutputData            interface{} `json:"outputData"`
	Logs                  interface{} `json:"logs"`
}

type ResponseStatus struct {
	OwnerApp   string `json:"ownerApp"`
	CreateTime int64  `json:"createTime"`
	UpdateTime int64  `json:"updateTime"`
	Status     string `json:"status"`
	EndTime    int64  `json:"endTime"`
	WorkflowID string `json:"workflowId"`
	Tasks      []struct {
		TaskType               string `json:"taskType"`
		Status                 string `json:"status"`
		ReferenceTaskName      string `json:"referenceTaskName"`
		RetryCount             int    `json:"retryCount"`
		Seq                    int    `json:"seq"`
		PollCount              int    `json:"pollCount"`
		TaskDefName            string `json:"taskDefName"`
		ScheduledTime          int64  `json:"scheduledTime"`
		StartTime              int64  `json:"startTime"`
		EndTime                int64  `json:"endTime"`
		UpdateTime             int64  `json:"updateTime"`
		StartDelayInSeconds    int    `json:"startDelayInSeconds"`
		Retried                bool   `json:"retried"`
		Executed               bool   `json:"executed"`
		CallbackFromWorker     bool   `json:"callbackFromWorker"`
		ResponseTimeoutSeconds int    `json:"responseTimeoutSeconds"`
		WorkflowInstanceID     string `json:"workflowInstanceId"`
		WorkflowType           string `json:"workflowType"`
		TaskID                 string `json:"taskId"`
		ReasonForIncompletion  string `json:"reasonForIncompletion"`
		CallbackAfterSeconds   int    `json:"callbackAfterSeconds"`
		WorkerID               string `json:"workerId"`
		OutputData             struct {
		} `json:"outputData"`
		RateLimitPerFrequency       int  `json:"rateLimitPerFrequency"`
		RateLimitFrequencyInSeconds int  `json:"rateLimitFrequencyInSeconds"`
		WorkflowPriority            int  `json:"workflowPriority"`
		Iteration                   int  `json:"iteration"`
		LoopOverTask                bool `json:"loopOverTask"`
		TaskDefinition              struct {
			Present bool `json:"present"`
		} `json:"taskDefinition"`
		TaskStatus    string `json:"taskStatus"`
		QueueWaitTime int    `json:"queueWaitTime"`
	} `json:"tasks"`
	WorkflowType  string `json:"workflowType"`
	Version       int    `json:"version"`
	SchemaVersion int    `json:"schemaVersion"`
	TaskToDomain  struct {
	} `json:"taskToDomain"`
	FailedReferenceTaskNames []string `json:"failedReferenceTaskNames"`
	Priority                 int      `json:"priority"`
	StartTime                int64    `json:"startTime"`
	WorkflowName             string   `json:"workflowName"`
	WorkflowVersion          int      `json:"workflowVersion"`
}

type InputCreateTask struct {
	Name                   string `json:"name"`
	RetryCount             int    `json:"retryCount"`
	RetryLogic             string `json:"retryLogic"`
	RetryDelaySeconds      int    `json:"retryDelaySeconds"`
	TimeoutSeconds         int    `json:"timeoutSeconds"`
	TimeoutPolicy          string `json:"timeoutPolicy"`
	ResponseTimeoutSeconds int    `json:"responseTimeoutSeconds"`
}

type InputCreateWF struct {
	Name          string `json:"name"`
	Description   string `json:"description"`
	Version       int    `json:"version"`
	SchemaVersion int    `json:"schemaVersion"`
	Tasks         []Task `json:"tasks"`
}

type Task struct {
	Name              string      `json:"name"`
	TaskReferenceName string      `json:"taskReferenceName"`
	InputParameters   interface{} `json:"inputParameters,omitempty"`
	Type              string      `json:"type"`
}

func WorkflowIsExist(conductorURL, name string) (bool, error) {
	client := &http.Client{Timeout: 10 * time.Second}
	req, err := http.NewRequest("GET", conductorURL+"/api/metadata/workflow/"+name, nil)
	if err != nil {
		return false, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return false, nil
	}

	return true, nil
}

func TaskIsExist(conductorURL, taskType string) (bool, error) {
	client := &http.Client{Timeout: 30 * time.Second}
	req, err := http.NewRequest("GET", conductorURL+"/api/metadata/taskdefs/"+taskType, nil)
	if err != nil {
		return false, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return false, nil
	}

	return true, nil

}

func CreateWF(conductorURL string, input InputCreateWF) error {
	requestBody, err := json.Marshal(input)

	resp, err := http.Post(conductorURL+"/api/metadata/workflow", "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		// handle error
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	glg.Info("create workflow on conductor success:", string(body))

	return nil
}

func CreateTask(conductorURL, taskname string, RetryCount, TimeoutSeconds, ResponseTimeoutSeconds int) error {

	temp := []InputCreateTask{InputCreateTask{Name: taskname,
		RetryCount:             RetryCount,
		RetryLogic:             "FIXED",
		RetryDelaySeconds:      10,
		TimeoutSeconds:         TimeoutSeconds,
		TimeoutPolicy:          "TIME_OUT_WF",
		ResponseTimeoutSeconds: ResponseTimeoutSeconds}}

	requestBody, err := json.Marshal(temp)

	resp, err := http.Post(conductorURL+"/api/metadata/taskdefs", "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		// handle error
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	glg.Info("create task on conductor success:", string(body))

	return nil
}

func PollTask(conductorURL, taskName string) (ResponsePoll, error) {
	client := &http.Client{Timeout: 30 * time.Second}
	req, err := http.NewRequest("GET", conductorURL+"/api/tasks/poll/"+taskName, nil)
	if err != nil {
		return ResponsePoll{}, err
	}

	res := ResponsePoll{}

	resp, err := client.Do(req)
	if err != nil {
		return ResponsePoll{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if len(body) != 0 {
		json.Unmarshal(body, &res)
		return res, nil
	}

	return ResponsePoll{}, nil
}

func RespToConductor(workflowInstanceId, taskId, reasonForIncomplete, workerId, status, conductorURL string, callbackAfterSeconds int, outputData, logs interface{}) error {

	temp := ResponseTask{WorkflowInstanceId: workflowInstanceId,
		TaskId:                taskId,
		ReasonForIncompletion: reasonForIncomplete,
		WorkerId:              workerId,
		Status:                status,
		CallbackAfterSeconds:  callbackAfterSeconds,
		OutputData:            outputData,
		Logs:                  logs}

	requestBody, err := json.Marshal(temp)

	resp, err := http.Post(conductorURL+"/api/tasks", "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		// handle error
		return err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	glg.Info("response to update task status to", status, "on conductor success:", string(body))

	return nil
}

func CheckWorkflowStatus(conductorURL, workflowID string) (ResponseStatus, error) {
	url := conductorURL + "/api/workflow/" + workflowID + "?includeTasks=true"
	client := &http.Client{Timeout: 30 * time.Second}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return ResponseStatus{}, err
	}

	res := ResponseStatus{}

	resp, err := client.Do(req)
	if err != nil {
		return ResponseStatus{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if len(body) != 0 {
		json.Unmarshal(body, &res)
		return res, nil
	}

	return ResponseStatus{}, nil
}

func ArchiveWorkflow(conductorURL, workflowInstanceId string) error {
	// body, err := json.Marshal(data)

	timeout := 10 * time.Second
	client := &http.Client{
		Timeout: timeout,
	}

	req, err := http.NewRequest("DELETE", conductorURL+fmt.Sprintf("/api/workflow/%s/remove?archiveWorkflow=true", workflowInstanceId), nil)
	if err != nil {
		// handle error
		return err
	}

	req.Header.Add("Accept", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		// handle error
		return err
	}
	defer resp.Body.Close()

	return nil
}

func StartWorkflow(conductorURL, workflowName string, version int, input interface{}) (string, error) {
	requestBody, err := json.Marshal(input)

	resp, err := http.Post(conductorURL+"/api/workflow/"+workflowName+"?version="+strconv.Itoa(version), "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		// handle error
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	glg.Info("response from start workflow", string(body))

	return string(body), nil
}
